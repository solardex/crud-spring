package com.baeldung.crud.secondrepo;

import java.util.List;

import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.baeldung.crud.seconddomain.Car;

public interface CarRepository extends JpaRepository<Car, Long> {

    // @Query("SELECT c FROM car c WHERE u.name LIKE %?1%"
    //         + " OR c.vendor LIKE %?1%"
    // )
    // public List<Car> contains(String keyword);

}
