package com.baeldung.crud.config;

import java.util.HashMap;

import javax.sql.DataSource;

import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

// @EnableJpaRepositories(basePackages = "com.baeldung.crud.secondrepo", entityManagerFactoryRef = "carEntityManager")
// @EnableJpaRepositories(basePackages = "com.baeldung.crud.secondrepo", entityManagerFactoryRef = "carEntityManager", transactionManagerRef = "productTransactionManager")
/**
 * By default, the persistence-multiple-db.properties file is read for 
 * non auto configuration in PersistenceProductConfiguration. 
 * <p>
 * If we need to use persistence-multiple-db-boot.properties and auto configuration 
 * then uncomment the below @Configuration class and comment out PersistenceProductConfiguration. 
 */
@Configuration
@PropertySource({"classpath:application.properties"})
@EnableJpaRepositories(basePackages = "com.baeldung.crud.repository", entityManagerFactoryRef = "utamaEntityManager")
@Profile("!tc")
public class UtamaDbConfig {

    @Autowired
    private Environment env;
    
    public UtamaDbConfig() {
        super();
    }

    @Primary
    @Bean
    public DataSource utamaDataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        // dataSource.setDriverClassName("org.h2.Driver");
        // dataSource.setUrl("jdbc:h2:file:./cardb");

        dataSource.setDriverClassName(Preconditions.checkNotNull(env.getProperty("spring.datasource.driverClassName")));
        dataSource.setUrl(Preconditions.checkNotNull(env.getProperty("spring.datasource.url")));
        dataSource.setUsername(Preconditions.checkNotNull(env.getProperty("spring.datasource.username")));
        // dataSource.setPassword(Preconditions.checkNotNull(env.getProperty("jdbc.pass")));
        return(dataSource);
    }


    @Primary
    @Bean
    public LocalContainerEntityManagerFactoryBean utamaEntityManager() {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(utamaDataSource());
        em.setPackagesToScan("com.baeldung.crud.domain");
        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        final HashMap<String, Object> properties = new HashMap<String, Object>();
        // properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        // properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.dialect", "org.hibernate.dialect.DerbyDialect");
        em.setJpaPropertyMap(properties);

        return em;
    }
}