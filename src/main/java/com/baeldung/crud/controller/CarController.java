package com.baeldung.crud.controller;

import javax.validation.Valid;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.ResponseBody;


import com.baeldung.crud.seconddomain.Car;
import com.baeldung.crud.secondrepo.CarRepository;

@Controller
@RequestMapping("/car")
public class CarController {

    @Autowired
    ApplicationContext context;

    @Autowired
    private DataSource dataSource;

	@Autowired
	CarRepository carRepository;
	
	SimpleDateFormat isoSdf = new SimpleDateFormat("yyyy-MM-dd");
	
    @GetMapping("/create")
    public String create(Car car) {
        return "car/create";
    }
    
    @PostMapping("/save")
    public String save(@Valid Car car, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "car/create";
        }
        
        carRepository.save(car);
        return "redirect:/car/index";
    }
    
    @GetMapping("/index")
    public String showUserList(Model model) {
        model.addAttribute("cars", carRepository.findAll());
        return "car/index";
    }
    
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") long id, Model model) {
        Car car = carRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid car Id:" + id));
        
        model.addAttribute("car", car);
        return "car/edit";
    }


    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") long id, @Valid Car car, 
      BindingResult result, Model model) {
        if (result.hasErrors()) {
            car.setId(id);
            return "car/edit";
        }
        carRepository.save(car);
        return "redirect:/car/index";
    }
        
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        Car car = carRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid car Id:" + id));
        carRepository.delete(car);
        return "redirect:/car/index";
    }
    
}
