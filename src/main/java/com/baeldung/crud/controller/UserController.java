package com.baeldung.crud.controller;

import javax.validation.Valid;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import com.baeldung.crud.domain.User;
import com.baeldung.crud.repository.UserRepository;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    ApplicationContext context;

    @Autowired
    private DataSource dataSource;

	@Autowired
	UserRepository userRepository;
	
	SimpleDateFormat isoSdf = new SimpleDateFormat("yyyy-MM-dd");
	
    @GetMapping("/signup")
    public String showSignUpForm(User user) {
        return "add-user";
    }
    
    @PostMapping("/adduser")
    public String addUser(@Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-user";
        }
        
        userRepository.save(user);
        return "redirect:/index";
    }
    
    @GetMapping("/index")
    public String showUserList(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "index";
    }
    
    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        
        model.addAttribute("user", user);
        return "update-user";
        
    }

    @GetMapping("/pdfc/{id}") @ResponseBody
    public void pdfc(@PathVariable("id") long id, HttpServletResponse response) throws Exception {
        // print pdf with compile .jrxml
        User user = userRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        
        // model.addAttribute("user", user);
        // return "update-user";

        Resource resource = context.getResource("classpath:reports/print-user.jrxml");
        //Compile to jasperReport
        InputStream inputStream = resource.getInputStream();
        JasperReport report = JasperCompileManager.compileReport(inputStream);
        Map<String, Object> reportParam = new HashMap<>();
        reportParam.put("pid", id);
        JasperPrint jasperPrint = JasperFillManager.fillReport(report, reportParam, dataSource.getConnection());
        
//        ByteArrayOutputStream  outputStream = new ByteArrayOutputStream();
//        JRExporter pdfExporter = new JRPdfExporter();
//        pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
//        pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
//        pdfExporter.exportReport();
        
        //Media Type
        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        //Export PDF Stream
        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());    
    }
    
    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid User user, 
      BindingResult result, Model model) {
        if (result.hasErrors()) {
            user.setId(id);
            return "update-user";
        }
        userRepository.save(user);
        return "redirect:/index";
    }
        
    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        userRepository.delete(user);
        return "redirect:/index";
    }
    
    @GetMapping("/search")
    public String searchUser(Model model) { 
    	model.addAttribute("users", userRepository.findAll());
        return "search";
    }

    @PostMapping("/search")
    public String searchUserResult(Model model, @Param("q") String q) {
    	model.addAttribute("q", q);
        model.addAttribute("users", userRepository.contains(q));
        return "search";
    }

    // @GetMapping("/login")
    // public void springSecurityLogin() {
        
    // }
}
