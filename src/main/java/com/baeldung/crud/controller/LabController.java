package com.baeldung.crud.controller;

import com.baeldung.crud.PlainPasswordEncoder;

import org.apache.commons.codec.digest.DigestUtils;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
// import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.data.repository.query.Param;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.ui.Model;

@Controller
@RequestMapping("/lab")
public class LabController {
	@GetMapping("/halo")
	public String halo() {
		return("lab/halo");
	}

	@GetMapping("/void")
	public void cobaVoid() {
		
	}

	@GetMapping("/list-var")
	public void listVar() {
		
	}

	@GetMapping("/encode")
	public void encode() {
		
	}

	@PostMapping("/encode")
	public void encodeResult(Model model, @Param("algo") String algo, @Param("str") String str) {
		model.addAttribute("str", str);
		String result = "";
		if (algo.equals("bcrypt")) {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			result = encoder.encode(str);
		}
		else if (algo.equals("noop")) {
			PlainPasswordEncoder encoder = new PlainPasswordEncoder();
			result = encoder.encode(str);
		}
		else if (algo.equals("pbkdf2")) {
			Pbkdf2PasswordEncoder encoder = new Pbkdf2PasswordEncoder();
			result = encoder.encode(str);
		}
		else if (algo.equals("sha256")) {
			// result = new DigestUtils("SHA3-256").digestAsHex(str);
			result = DigestUtils.sha256Hex(str);
		}
		
		model.addAttribute("result", result);
	}

}
